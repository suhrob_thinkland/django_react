from django.urls import path

from resumes.views import index, UserProfile

urlpatterns = [
    path('', index),
    path('api/user/me/', UserProfile.as_view()),
]
