import React, {useEffect, useState} from 'react';
import {createRoot} from "react-dom/client";

function App() {
    const [user, setUser] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch("/api/user/me/");
                const jsonData = await response.json();
                setUser(jsonData);
                console.log(jsonData);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, []);

    return (
        <>
            {
                user ? (
                    <h1>Hello, {user.username}</h1>
                ) : (
                    <h1>Loading...</h1>
                )
            }
        </>
    );
}

const root = createRoot(document.getElementById('app'));
root.render(<App/>);